import React from 'react';
import AppNavigation from './app/AppNavigation';
import reduxStore from './app/config/reduxStore';
import { Provider } from 'react-redux';
import { Root } from "native-base";

export default function App() {
  return (
    <Root>
      <Provider store={reduxStore}>
        <AppNavigation />
      </Provider>
    </Root>
  );
}
