import React from 'react';
import { StyleSheet, AsyncStorage } from 'react-native';
import { Container, Content, Button, Text, Toast } from 'native-base';
import AuthFormContainer from './../containers/AuthFormContainer';
import { useSelector } from 'react-redux';
import Axios from 'axios';

export default SignIn = ({ navigation }) => {

    const email = useSelector(({ auth }) => auth.email);
    const password = useSelector(({ auth }) => auth.password);

    const signIn = () => {
        if (!email || !password) {
            Toast.show({
                text: 'Email and password is required',
                buttonText: 'Ok'
            })
        }

        const data = {
            email, password
        }

        if (email === 'John@gmail.com' && password === '123456') {
            Axios.post(
                'https://run.mocky.io/v3/1ca7ea2b-933f-4747-8483-e57e6e58fd00',
                data
            ).then(async res => {
                const { code } = res.data;

                if (code === 200) {
                    const { data: { token } } = res.data;

                    storeAccessToken(token);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Chat Room Listing' }],
                    });
                }

            }).catch(err => {
                console.log('Sign in error: ', err);
            })
        }
    }

    const storeAccessToken = async (token) => {
        await AsyncStorage.setItem('@access_token', token);
    }
    
    return (
        <Container>
            <Content style={styles.contentContainer}>
                <AuthFormContainer title="Sign In" />

                <Button info rounded
                    style={styles.button}
                    onPress={signIn}>
                    <Text>Sign In</Text>
                </Button>
                <Button transparent rounded
                    style={[styles.button, styles.secondaryButton]}
                    onPress={() => navigation.navigate('Sign Up')}>
                    <Text>Sign Up</Text>
                </Button>
            </Content>
        </Container>
    )
}

const styles = StyleSheet.create({
    contentContainer: {
        paddingHorizontal: 30,
    },
    button: {
        flex: 1,
        marginBottom: 10,
    },
});
  