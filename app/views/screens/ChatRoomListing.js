import React, { useEffect, useState } from 'react';
import { Container, Content, List } from 'native-base';
import Axios from 'axios';
import ChatRoomListItem from './../components/ChatRoomListItem';

export default ChatRoomListing = ({ navigation }) => {

    const [chatRoomListing, setChatRoomListing] = useState([]);

    useEffect(() => {
        Axios.get(
            'https://run.mocky.io/v3/5bad9edc-60b6-42e1-bbbc-c722e4853eda',
        ).then(res => {
            const { code } = res.data;
            
            if (code === 200) {
                const { data } = res.data;
                setChatRoomListing(data);
            }
        }).catch(err => {
            console.log("Get chat room listing error: ", err);
        })
    }, []);

    return (
        <Container>
            <Content>
                <List>
                    {chatRoomListing.length > 0 && chatRoomListing.map((chatRoom, index) => {
                        const { image, name, lastMessageTimestamp } = chatRoom;

                        return (
                            <ChatRoomListItem
                                key={index}
                                image={image}
                                name={name}
                                lastMessageTimestamp={lastMessageTimestamp}
                            />
                        )
                    })}
                </List>
            </Content>
        </Container>
    )
}
  