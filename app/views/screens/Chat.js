import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Item, Input, View, Button } from 'native-base';
import SenderMessage from './../components/SenderMessage';
import ReceiverMessage from './../components/ReceiverMessage';
import moment from 'moment';
import * as ImagePicker from 'expo-image-picker';

export default Chat = ({ route, navigation }) => {

    const scrollViewRef = useRef();

    const [inputMessage, setInputMessage] = useState("");
    const [messages, setMessages] = useState([]);
    const [triggerReply, setTriggerReply] = useState(false);

    useEffect(() => {
        const { name } = route.params;
        navigation.setOptions({ title: name });
    }, [])

    useEffect(() => {
        if (triggerReply) {
            setTriggerReply(false);

            // To simulate reply
            setTimeout(() => {
                const replyMessage = {
                    'sender': false,
                    'message': "This is auto generated reply",
                    'timestamp': moment()
                }

                setMessages(messages.concat(replyMessage));
            }, 3000);
        }
        
    }, [triggerReply]);

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            const img = result.uri;

            const sendMessage = {
                'sender': true,
                'message': img,
                'timestamp': moment(),
                'is_image': true
            }
    
            setMessages(messages.concat(sendMessage));
            setTriggerReply(true);
        }
    };

    const sendMessage = () => {
        if (!inputMessage) return;

        const sendMessage = {
            'sender': true,
            'message': inputMessage,
            'timestamp': moment()
        }

        setMessages(messages.concat(sendMessage));

        setInputMessage('');
        setTriggerReply(true);
    }
    
    return (
        <Container style={styles.container}>

            <ScrollView 
                style={styles.messageContainer} 
                contentContainerStyle={{ paddingBottom: 15 }}
                ref={scrollViewRef}
                onContentSizeChange={(contentWidth, contentHeight)=> {scrollViewRef.current.scrollToEnd({animated: true})}}
                >
                {
                    messages.map((messageItem, index) => {
                        const { sender, message, timestamp, is_image } = messageItem;

                        if (sender) {
                            return (
                                <SenderMessage
                                    key={index}
                                    message={message}
                                    timestamp={timestamp}
                                    is_image={is_image}
                                />
                            )
                        }
                        else {
                            return (
                                <ReceiverMessage
                                    key={index}
                                    message={message}
                                    timestamp={timestamp}
                                    is_image={is_image}
                                />
                            )
                        }
                    })
                }
            </ScrollView>

            <View style={styles.inputContainer}>
                <Button transparent
                    style={styles.inputButton}
                    onPress={pickImage}>
                    <Ionicons name="attach" size={40} color="black" />
                </Button>

                <Item style={styles.input}>
                    <Input
                        placeholder="Type a message"
                        value={inputMessage}
                        onChangeText={(text) => setInputMessage(text)} />
                </Item>

                <Button transparent
                    style={styles.inputButton}
                    onPress={sendMessage}>
                    <Ionicons name="send" size={40} color="black" />
                </Button>
                
            </View>
        </Container>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    messageContainer: {
        flex: 1,
        padding: 15,
    },
    inputContainer: {
        height: 60,
        borderTopWidth: 1,
        borderTopColor: 'lightgrey',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    inputButton: {
        width: 50,
        height: 60,
        padding: 5,
    },
    input: {
        flex: 1
    }
});