import React from 'react';
import { StyleSheet } from 'react-native';
import { Item, Input, View, Text, Label } from 'native-base';
import { Entypo } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import { authActions } from '../../states/auth';

export default AuthFormContainer = ({ title }) => {

    const dispatch = useDispatch();
    const setEmail = (email) => dispatch(authActions.setEmail(email));
    const setPassword = (password) => dispatch(authActions.setPassword(password));

    const email = useSelector(({ auth }) => auth.email);
    const password = useSelector(({ auth }) => auth.password);
    
    return (
        <View>
            <View style={styles.logoContainer}>
                <Entypo name="chat" size={50} color="black" />
                <Text>Intrinsik OMS</Text>
            </View>

            <View style={styles.formContainer}>
                <Text style={styles.title}>{title}</Text>
                <Item floatingLabel 
                    style={styles.inputItem}>
                    <Label>Email</Label>
                    <Input 
                        placeholder="Email"
                        value={email}
                        keyboardType="email-address"
                        onChangeText={(text) => setEmail(text)} />
                </Item>

                <Item floatingLabel 
                    style={styles.inputItem}>
                    <Label>Password</Label>
                    <Input
                        placeholder="Password"
                        value={password}
                        secureTextEntry={true}
                        onChangeText={(text) => setPassword(text)} />
                </Item>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 60,
        marginTop: 30,
    },
    formContainer: {
        marginBottom: 30,
    },
    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 20,
    },
    inputItem: {
        marginBottom: 10,
    }
});
  