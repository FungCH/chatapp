import React from 'react';
import { ListItem, Text, Left, Body, Right } from 'native-base';
import { Image, StyleSheet } from 'react-native';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';

export default ChatRoomListItem = ({ image, name, lastMessageTimestamp }) => {

    const navigation = useNavigation();
    const formattedLastMessageTimestamp = moment(lastMessageTimestamp).fromNow();

    const goToChat = () => {
        navigation.navigate('Chat', {
            'name': name
        });
    }

    return (
        <ListItem style={styles.listItemContainer} onPress={goToChat}>
            <Left style={styles.left}>
                <Image source={{uri: image}} style={styles.image}/>
            </Left>
            <Body style={styles.body}>
                <Text style={{ textAlign: 'left' }}>{name}</Text>
            </Body>
            <Right style={styles.right}>
                <Text style={styles.timestamp}>{formattedLastMessageTimestamp}</Text>
            </Right>
        </ListItem>
    )
}

const styles = StyleSheet.create({
    listItemContainer: {
        marginLeft: 0,
        paddingLeft: 10,
    },
    image: {
        borderRadius: 50,
        width: 40,
        height: 40,
    },
    left: {
        flex: 1,
    },
    body: {
        flex: 5,
    },
    right: {
        flex: 1.5,
    },
    timestamp: {
        fontSize: 10,
        color: 'grey',
    }
});