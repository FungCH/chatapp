import React from 'react';
import { View, Text } from 'native-base';
import { StyleSheet } from 'react-native';
import moment from 'moment';

export default ReceiverMessage = ({ message, timestamp, is_image }) => {

    const formattedTimestamp = moment(timestamp).fromNow();

    return (
        <View style={styles.container}>
            <View style={styles.senderMessageContainer}>
                {
                    is_image 
                    ? <Image source={{uri: message}} style={styles.image} />
                    : <Text style={styles.message}>{message}</Text>
                }
                <Text style={styles.timestamp}>{formattedTimestamp}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: "flex-start"
    },
    senderMessageContainer: {
        borderRadius: 10,
        borderBottomLeftRadius: 0,
        backgroundColor: 'lightblue',
        padding: 10,
        marginBottom: 15,
        maxWidth: 300,
    },
    message: {
        fontSize: 16,
    },
    timestamp: {
        fontSize: 10,
        color: 'grey',
        textAlign: 'right'
    },
});