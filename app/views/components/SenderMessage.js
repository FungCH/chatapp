import React from 'react';
import { View, Text } from 'native-base';
import { StyleSheet, Image } from 'react-native';
import moment from 'moment';

export default SenderMessage = ({ message, timestamp, is_image }) => {

    const formattedTimestamp = moment(timestamp).fromNow();
    
    return (
        <View style={styles.container}>
            <View style={styles.senderMessageContainer}>
                {
                    is_image 
                    ? <Image source={{uri: message}} style={styles.image} />
                    : <Text style={styles.message}>{message}</Text>
                }
                <Text style={styles.timestamp}>{formattedTimestamp}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: "flex-end"
    },
    senderMessageContainer: {
        borderRadius: 10,
        borderBottomRightRadius: 0,
        backgroundColor: 'lightgreen',
        padding: 10,
        marginBottom: 15,
        maxWidth: 300,
    },
    message: {
        fontSize: 16,
    },
    timestamp: {
        fontSize: 10,
        color: 'grey',
        textAlign: 'right'
    },
    image: {
        width: '100%',
        height: undefined,
        aspectRatio: 1,
    }
});