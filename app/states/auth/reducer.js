import * as types from './types';

const initialState = {
    email: 'John@gmail.com',
    password: '123456',
}

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_EMAIL: {
            return {
                ...state,
                email: action.payload.email
            }
        }
        case types.SET_PASSWORD: {
            return {
                ...state,
                password: action.payload.password
            }
        }
        default:
            return state;
    }
}

export default AuthReducer