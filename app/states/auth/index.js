import AuthReducer from './reducer';
import * as authTypes from './types';
import * as authActions from './actions';

export {
    authTypes, authActions
}

export default AuthReducer;