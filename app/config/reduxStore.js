import { createStore } from 'redux';
import rootReducer from '../states';

export default store = createStore(
    rootReducer
);