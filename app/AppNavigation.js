import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from './views/screens/SignIn';
import SignUp from './views/screens/SignUp';
import ChatRoomListing from './views/screens/ChatRoomListing';
import Chat from './views/screens/Chat';

const Stack = createStackNavigator();

export default AppNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Sign In">
        <Stack.Screen name="Sign In" component={SignIn} />
        <Stack.Screen name="Sign Up" component={SignUp} />
        <Stack.Screen name="Chat Room Listing" component={ChatRoomListing} headerLeft={null} />
        <Stack.Screen name="Chat" component={Chat} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
